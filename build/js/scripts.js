(function ($) {

	'use strict';

	// COMBO SUBSCRIBE-NAME
	$('.menu-job li a').click( function() {
		var value = $(this).text();
		$(this).parents('.btn-group').find('.dropdown-toggle').html(value+ ' <i class="fa fa-angle-down"></i>');
	});

})(jQuery);